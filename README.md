# Introduction
All code required to control the PBv3 testing setup.

[API documentation](https://labremote.web.cern.ch/labRemote/apps/powertools)

# Quick Start
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/berkeleylab/labremote-apps/powertools.git
cd powertools
mkdir build
cd build
cmake3 ..
make
```

# Repository Structure
 - `labremote`: submodule with LabRemote library
 - `pbv3`: source code for the `PBv3` library
 - `pbv3/tools`: programs for controlling and monitoring Powerboards
 - `scripts`: helpful bash scripts

# Hardware Configuration File

Powertools uses a hardware configuration file from [labRemote](http://labremote.web.cern.ch) to define any attached power supplies and the Powerboard testbench. An example file is provided under `config/equip_testbench.json` with template entries for single board, mass tester and module configurations. The desired configuration file can be passed to all powertools programs using the `-e` option.

## Power Supplies
See the [libPS documentation on how to setup labRemote power supplies](http://labremote.web.cern.ch/labRemote/master/md__builds_berkeleylab_labRemote_src_configs_README.htm).

The high and low voltage power supplies can be specified as the `Vin` and `HVin` channels. Specifying them is optional for most usage. They are used for:
1. Remote control using the `powersupply` command.
2. Monitoring of power usage (skipped if not defined).
3. QC tests.

## Powerboard Testbench
Communication with a Powerboard is abstracted through a powerboard testbench (`PBv3TB`). A testbench is defined as a collection of the following hardware:
 - 1x low voltage power supply
 - 1x high voltage power supply
 - *N*x loads connected to powerboards
 - *N*x powerboards
 - 1x ADC connected to the AMACv2 CAL line
 
 All hardware, other than the Powerboards, is optional. For example, there is no load in a module setup. Also the LV and HV power supplies are optional for module operation, but recommended for the monitoring tools.
 
 The testbench is configured via the hardware configuration file. The testbench named as `default` is then used by all the tools. For example, the hardware configuration file for module operations should contain the following:
 ```json
    "testbenches": {
        "default": {
            "type"  : "PBv3TBModule",
            "ip"    : "192.168.222.16",
            "port"  : 60003
        }
    }
 ```

# Powerboard Configuration
The Powerboard configuration consists of register values and the corresponding calibration values. The configuration is stored inside a JSON file that follows the schema of the `CONFIG` test type in the production database.

The following keys are required in the top-level (or stored under `"config"`) dictionary.
- `properties`: dictionary where the keys are AMAC field names and the values the corresponding settings.
- `results`: dictionary with different calibration values

Optional keys:
- `testType`: `"CONFIG"` to indicate the test type when uploading the configuration to the database
- `runNumber`: Run number for the test, used to link test results in the ITk-PD
- `component`: Serial number for the Powerboard. Used when uploading the configuration to the ITk-PD. Also useful when analysing test results to identify the Powerboard under test.

In principle, the recommended register settings and the corresponding calibration can be downloaded from the production database. However some extra re-formatting is necessary as the input and output formats in the ITk-PD are slightly different. The format used by powertools corresponds to the input format.
