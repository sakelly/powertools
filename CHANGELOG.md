# v2.0.0
- Support for AMACstar. 
- Require an institution argument for mass test.
# v1.10.0
- Update TEMPERATURE test settings and pass ranges.
# v1.9.0
- Add invertCMDin/out settings to PBv3TBMassive20210504 to explicitly set polarity.
# v1.8.0
- Update labRemote tag with installation of tools.
# v1.7.0
- Add powersupply initialization and I2C commuication checks (`pbv3_stand_by`)
# v1.6.1
- Fixed Keithley 24xx current range setting in labRemote
